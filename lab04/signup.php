<?php include "top.html"; ?>
<form action="signup-submit.php" method="post">
<fieldset>

    <legend>New User Signup</legend>

    <p><strong>Name:</strong> <label><input type="text" size="16" maxlength="16" name="name"></label></p>

    <p><strong>Gender:</strong> 
        <label><input type="radio" name="gender" value="M"> Male</label>
        <label><input type="radio" name="gender" value="F" checked="checked"> Female</label>
    </p>

    <p><strong>Age:</strong> <label><input type="text" size="6" maxlength="2" name="age"></label> </p>

    <p><strong>Personality Type:</strong> <label><input type="text" size="6" maxlength="4" name="ptype"> </label>
    <a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp"><span style="color:red">(Don't know your type?)</span></a></p>

    <p><strong>Favorite OS:</strong>
        <label><select name="favos">
            <option>Windows</option>
            <option>Mac OS X</option>
            <option selected="selected">Linux</option>
        </select></label>
    </p>

    <p><strong>Seeking age:</strong>
        <label><input type="text" size="6" maxlength="2" placeholder="min" name="minage"></label>
        <label><input type="text" size="6" maxlength="2" placeholder="max" name="maxage"></label>
    </p>

    <p><strong>Seeking Gender:</strong> 
        <label><input type="radio" name="seekinggender" value="M"> Male</label>
        <label><input type="radio" name="seekinggender" value="F"> Female</label>
        <label><input type="radio" name="seekinggender" value="MF" checked="checked"> Both</label>
    </p>

    <input type="submit" value="Sign Up"> 

</fieldset>
</form>
<?php include "bottom.html"; ?>