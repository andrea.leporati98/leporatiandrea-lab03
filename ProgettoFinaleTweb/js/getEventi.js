// Funzione per recuperare gli eventi dal server
function getEventi() {
    $.ajax({
        url: "../php/server/homepageServer.php",
        type: "GET",
        success: function (data) {
            var eventi = JSON.parse(data);
            var eventListContainer = $("#eventListContainer");

            // Svuota il contenuto dell'elemento HTML degli eventi
            eventListContainer.empty();

            // Visualizza gli eventi nella pagina
            for (var i = 0; i < eventi.length; i++) {
                var evento = eventi[i];
                var eventHTML = "<div class='evento'>" +
                    "<form class='evento-form'>" +
                    "<h3><label>Nome evento: <input type='text' name='nomeEvento' value='" + evento.NomeEvento + "'></label></h3>" +
                    "<input type='hidden' name='eventoID' value='" + evento.eventoID + "'>" +
                    "<p><label>Locandina: <input type='text' name='pathLocandina' value='" + evento.pathLocandina + "'></label></p>" +
                    "<p><label>Descrizione: <textarea name='descrizioneEvento'>" + evento.DescrizioneEvento + "</textarea></label></p>" +
                    "<p><label>Data: <input type='date' name='dataEvento' value='" + evento.DataEvento + "'></label></p>" +
                    "<p><label>Posti disponibili: <input type='number' name='postiDisponibili' value='" + evento.PostiDisponibili + "'></label></p>" +
                    "<p><label>Posti totali: <input type='number' name='postiTotali' value='" + evento.PostiTotali + "'></label></p>" +
                    "<p><label>Luogo: <input type='text' name='luogoEvento' value='" + evento.LuogoEvento + "'></label></p>" +
                    "<button type='submit'>Aggiorna</button>" +
                    "</form>" +
                    "</div>";
            
                eventListContainer.append(eventHTML);
            }
            
        },
        error: function (xhr, status, error) {
            console.log("Errore durante il recupero degli eventi.");
        }
    });
}

// Richiama la funzione per ottenere gli eventi al caricamento della pagina
getEventi();

$(document).ready(function () {
    // Gestisci il submit del form di aggiornamento evento
    $("body").on("submit", ".evento-form", function (event) {
        event.preventDefault(); // Previene il comportamento predefinito di invio del form

        var form = $(this);
        var eventoID = form.find("input[name='eventoID']").val();

        // Ottieni i dati del form
        var data = form.serialize();

        // Invia la richiesta AJAX per l'aggiornamento dei dati dell'evento
        $.ajax({
            url: "../php/server/updateEvent.php",
            type: "POST",
            data: data,
            success: function (response) {
                if (response.status === "success") {
                    console.log("Dati evento aggiornati con successo.");
                } else if (response.status === "error") {
                    console.log("Errore durante l'aggiornamento dei dati dell'evento.");
                    // Mostra un messaggio all'utente
                    if (response.message) {
                        alert(response.message);
                    }
                }
            },
            error: function (xhr, status, error) {
                console.log("Errore durante l'aggiornamento dei dati dell'evento.");
            }
        });
    });
});
