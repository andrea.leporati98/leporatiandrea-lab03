$(document).ready(function () {
    $('#user').on('click', function () {
        $('#popUpUser').toggle();
    });

    $('#logOut').on('click', function () {
        // Effettua una richiesta AJAX per terminare la sessione
        $.ajax({
            url: '../php/server/logout.php',
            type: 'POST',
            success: function () {
                window.location.href = '../php/index.php';
            }
        });
    });

    $('#suggerimentoForm').submit(function (event) {
        event.preventDefault(); // Previene il comportamento predefinito di invio del form

        var testo = $('#suggestion').val();

        $.ajax({
            url: "../php/server/homepageServer.php",
            type: "POST",
            data: {
                testo: testo
            },
            success: function (response) {
                $('#suggestion').val(''); // Svuota la textarea dopo l'invio di successo
                // Puoi anche visualizzare un messaggio di successo se necessario
            },
            error: function (xhr, status, error) {
                $('#suggestion').val(''); // Svuota la textarea in caso di errore
                alert("Errore: Impossibile inviare il suggerimento. Riprova più tardi."); // Messaggio di errore generico
            }
        });
    });

});