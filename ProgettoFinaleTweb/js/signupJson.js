$(document).ready(function () {
    $('#requisitiPassword').on('click', function () {
        $('#myPopup').toggle();
    });

    $('#signup').on('click', function () {
        $("#signup").attr("disabled", "disabled");
        var nome = $('#nome').val();
        var cognome = $('#cognome').val();
        var mailPrefix = $('#mail_prefix').val();
        var mail = mailPrefix + '@allaquinta.it';
        var pw = $('#pw').val();
        var pw2 = $("#pw2").val();
        var phone = $('#phone').val();
        var annoNascita = $('#annoNascita').val();
        var ruolo = $('#ruolo').val();
        const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;





        if (pw.length < 8 || pw.length > 16 || hasNumber(pw) == false || specialChars.test(pw) == false) {
            $("#message").text("La Password deve rispettare i requisiti richiesti");
            console.log("false");
            $("#signup").removeAttr("disabled"); // Riabilita il pulsante Signup
            return false;
        } else if (pw != pw2) {
            $("#message").text("Le due Password devono essere identiche");
            console.log("false");
            $("#signup").removeAttr("disabled"); // Riabilita il pulsante Signup
            return false;
        } else if (checkPhoneNumber(phone) === false) {
            $("#message").text("Inserisci un numero di telefono valido");
            console.log("false");
            $("#signup").removeAttr("disabled"); // Riabilita il pulsante Signup
            return false;
        } else if (checkBirthYear(annoNascita) === false) {
            $("#message").text("Inserisci un anno di nascita valido");
            console.log("false");
            $("#signup").removeAttr("disabled"); // Riabilita il pulsante Signup
            return false;
        } else {
            $.ajax({
                url: "../php/server/signup-submit.php",
                type: "POST",
                data: {
                    nome: nome,
                    cognome: cognome,
                    mail: mail,
                    pw: pw,
                    phone: phone,
                    annoNascita: annoNascita,
                    ruolo: ruolo
                },
                success: function (dataResult) {
                    if (dataResult.status == "1") {
                        $('#myPopup').hide();
                        $("#signup").removeAttr("disabled");
                        $('#registration').find('input').prop("disabled", true);
                        $('#requisitiPassword').off("click");
                        $('#divMessaggioRegistrazione').show();
                        $('#testoMessaggioSignUp').text("Registrazione eseguita!!! Benvenut* " + nome + " " + cognome + ". Puoi andare alla pagina per eseguire una prenotazione premendo il bottone sottostante.");
                        $('#redirectLogin').show();
                    } else if (dataResult.status == "0") {
                        $('#myPopup').hide();
                        $("#signup").removeAttr("disabled");
                        $('#registration').find('input').prop("disabled", true);
                        $('#requisitiPassword').off("click");
                        $('#divMessaggioRegistrazione').show();
                        $('#testoMessaggioSignUp').text("Mail già esistente!");
                        $('#redirectSignUp').show();
                    }
                }
            });
        }
    });
});

function checkPhoneNumber(phone) {
    // Verifica se il numero di telefono ha un formato valido (es. +39 1234567890)
    var phonePattern = /^(\+)?(\d{1,3})?(\s)?(\d{9,15})$/;
    return phonePattern.test(phone);
}


function checkBirthYear(year) {
    // Verifica se l'anno di nascita è compreso tra 1900 e l'anno corrente
    var currentYear = new Date().getFullYear();
    var birthYear = parseInt(year);
    return Number.isInteger(birthYear) && birthYear >= 1900 && birthYear <= currentYear;
}

function hasNumber(myString) {
    return /\d/.test(myString);
}
