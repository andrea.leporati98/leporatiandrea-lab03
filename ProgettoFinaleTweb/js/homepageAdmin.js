
$(document).ready(function () {
  $('#user').on('click', function () {
    $('#popUpUser').toggle();
  });

  $('#logOut').on('click', function () {
    // Effettua una richiesta AJAX per terminare la sessione
    $.ajax({
      url: '../php/server/logout.php',
      type: 'POST',
      success: function () {
        window.location.href = '../php/index.php';
      }
    });
  });

  function getSuggerimenti() {
    $.ajax({
      url: "../php/server/homepageAdminServer.php",
      type: "GET",
      success: function (data) {
        // Elabora i dati ricevuti
        var suggerimenti = JSON.parse(data);

        // Visualizza i suggerimenti nella pagina
        for (var i = 0; i < suggerimenti.length; i++) {
          var mail = suggerimenti[i].mail;
          var testo = suggerimenti[i].testo;

          // Crea un elemento HTML per il suggerimento
          var suggerimentoHTML = "<div class='suggerimento'>" +
            "<p class='mail'> User: " + mail + "</p>" +
            "<p class='testo'> Ha suggerito: " + testo + "</p>" +
            "</div>";

          // Aggiungi il suggerimento alla sezione corrispondente nella pagina
          $("#suggerimentiContainer").append(suggerimentoHTML);
        }
      },
      error: function (xhr, status, error) {
        console.log("Errore durante il recupero dei suggerimenti.");
      }
    });
  }

  // Richiama la funzione per ottenere i suggerimenti al caricamento della pagina
  getSuggerimenti();

  $('#createEventForm').submit(function (e) {
    e.preventDefault(); // Impedisce il comportamento predefinito di invio del form

    // Recupera i valori dai campi del form
    var eventName = $('#eventName').val();
    var eventPoster = $('#eventPoster').val();
    var eventDescription = $('#eventDescription').val();
    var eventDate = $('#eventDate').val();
    var totalSeats = $('#totalSeats').val(); // Aggiunta: Recupera il valore di totalSeats
    var eventLocation = $('#eventLocation').val();
    var responsableEmail = $('#responsableEmail').val();


    var availableSeats = totalSeats;
    // Aggiungi "../img/" al valore della locandina
    var posterPath = "../img/" + eventPoster;
    // Assegna il nuovo valore all'input della locandina
    $("#eventPoster").val(posterPath);

    // Crea un oggetto FormData per inviare i dati del form
    var formData = new FormData();
    formData.append('eventName', eventName);
    formData.append('eventPoster', posterPath); // Utilizza il nuovo valore modificato
    formData.append('eventDescription', eventDescription);
    formData.append('eventDate', eventDate);
    formData.append('availableSeats', availableSeats);
    formData.append('totalSeats', totalSeats); // Aggiunta: Includi totalSeats
    formData.append('eventLocation', eventLocation);
    formData.append('responsableEmail', responsableEmail);

    // Invia la richiesta AJAX al server
    $.ajax({
        url: '../php/server/homepageAdminServer.php',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            // Gestisci la risposta del server
            if (response.status === 'success') {
                // Operazione completata con successo
                console.log('Evento creato con successo');
            } else {
                // Errore durante la creazione dell'evento
                console.log('Si è verificato un errore durante la creazione dell\'evento');
            }
        },
        error: function (xhr, status, error) {
            // Errore durante la richiesta AJAX
            console.log('Si è verificato un errore durante la richiesta AJAX:', error);
        }
    });
});

  // Funzione per ottenere la lista di email uniche dal server
  function getEmails() {
    $.ajax({
      url: "../php/server/getEmails.php",
      type: "GET",
      success: function (data) {
        var emails = JSON.parse(data);
        var uniqueEmails = [];

        // Filtra le email uniche
        $.each(emails, function (index, email) {
          if ($.inArray(email, uniqueEmails) === -1) {
            uniqueEmails.push(email);
          }
        });

        // Rimuovi le opzioni esistenti
        $("#responsableEmail").empty();

        // Aggiungi le nuove opzioni
        $.each(uniqueEmails, function (index, email) {
          var option = $("<option>").text(email).val(email);
          $("#responsableEmail").append(option);
        });
      },
      error: function (xhr, status, error) {
        console.log("Errore durante il recupero delle email.");
      }
    });
  }

  // Richiama la funzione per ottenere le email al caricamento della pagina
  $(document).ready(function () {
    getEmails();
  });

  


});

