$(document).ready(function () {
  // Funzione per ottenere i dati degli eventi dal server
  function getEventi() {
    $.ajax({
      url: "../php/server/getEventi.php",
      type: "GET",
      success: function (data) {
        var eventi = JSON.parse(data);

        // Itera sugli eventi e crea le carte
        for (var i = 0; i < eventi.length; i++) {
          var evento = eventi[i];
          var nomeEvento = evento.NomeEvento;
          var pathLocandina = evento.pathLocandina;
          var dataEvento = evento.DataEvento;
          var postiDisponibili = evento.PostiDisponibili;
          var postiTotali = evento.PostiTotali;
          var descrizioneEvento = evento.DescrizioneEvento; // Aggiunto il recupero della descrizione

          // Crea la carta dell'evento
          var cardHTML = $('<div>').addClass('card');
          var img = $('<img>').attr('src', pathLocandina).attr('alt', 'Locandina evento');
          var h3 = $('<h3>').text(nomeEvento);
          var p1 = $('<p>').text('Data: ' + dataEvento);
          var p2 = $('<p>').text('Posti disponibili: ' + postiDisponibili + ' / ' + postiTotali);
          var p3 = $('<p>').text('Descrizione: ' + descrizioneEvento); // Aggiunta la descrizione

          // Aggiungi gli elementi al container della carta
          cardHTML.append(img, h3, p1, p2, p3);

          // Aggiungi la carta al contenitore interno dello slider
          $('.inner-slider').append(cardHTML);
        }
      },
      error: function (xhr, status, error) {
        console.log("Errore durante il recupero degli eventi.");
      }
    });
  }

  // Richiama la funzione per ottenere gli eventi al caricamento della pagina
  getEventi();



  // Gestisci il submit del form di prenotazione
  $("#myForm").submit(function (event) {
    event.preventDefault(); // Previene il comportamento predefinito di invio del form

    // Recupera i valori dal form
    var nome = $("#nome").val();
    var cognome = $("#cognome").val();
    var mail = $("#mail").val();
    var telefono = $("#telefono").val();
    var annoNascita = $("#annoNascita").val();
    var idPartecipazioneEvento = $("#evento").val();
    var posti = parseInt($("#posti").val());

    // Recupera il numero di posti disponibili dell'evento selezionato
    var postiDisponibili = parseInt($("#evento option:selected").data("posti-disponibili"));

    // Verifica se il numero di posti selezionati è valido
    if (posti <= postiDisponibili) {
      // Crea un oggetto FormData per inviare i dati del form
      var formData = new FormData();
      formData.append("nome", nome);
      formData.append("cognome", cognome);
      formData.append("mail", mail);
      formData.append("telefono", telefono);
      formData.append("annoNascita", annoNascita);
      formData.append("idPartecipazioneEvento", idPartecipazioneEvento);
      formData.append("posti", posti);

      console.log("Numero di posti:", posti);
      console.log("Posti disponibili:", postiDisponibili);

      // Invia la richiesta AJAX al server
      $.ajax({
        url: "../php/server/inviaPrenotazione.php",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {


          alert("Prenotazione inviata con successo.");
          console.log("Prenotazione inviata con successo.");
        },
        error: function (xhr, status, error) {
          console.log("Si è verificato un errore durante la richiesta AJAX:", error);
        }
      });
    } else {
      console.log("Il numero di posti selezionati supera i posti disponibili.");
    }
  });
});
