$(document).ready(function(){
    $('#formLogIn').on("submit", function(event){
        event.preventDefault();

        var mail = $('#mail').val();
        var pw = $('#pw').val();
        $.ajax({
            url:"../php/server/login.php",
            type: "POST",
            data: {
                mail: mail,
                pw: pw
            },
            success: function(result){
                
                if(result.risposta == "successo login") {
                    if (result.ruolo == "personale") window.location = "../php/homepage.php";
                    else if (result.ruolo == "admin") window.location = "../php/homepageAdmin.php";
                }
                else if (result.risposta == "utente non registrato") {
                    $('#pw').val('');
                    $('#message').text("Questa mail non è presente nel database, se desideri aggiungerla clicca sulla scritta sottostante Registrati Ora");
                    $('#message').show();
                }
                else if (result.risposta == "password errata"){
                    $('#pw').val('');
                    $('#message').text("La Password non corrisponde!");
                    $('#message').show();
                }
            }
        });
    });
});