// Funzione per ottenere gli eventi disponibili dal server
function getEventiDisponibili() {
    $.ajax({
        url: "../php/server/getEventiDisponibili.php", // Inserisci l'URL del tuo script PHP per ottenere gli eventi disponibili
        type: "GET",
        success: function (data) {
            var eventi = JSON.parse(data);

            // Popola le opzioni degli eventi nel form
            var selectElement = $("#evento");
            selectElement.empty();

            for (var i = 0; i < eventi.length; i++) {
                var evento = eventi[i];
                var nomeEvento = evento.NomeEvento;
                var eventoID = evento.eventoID;
                var postiDisponibili = evento.PostiDisponibili;

                var optionElement = $("<option>").text(nomeEvento).attr("value", eventoID).data("posti-disponibili", postiDisponibili);
                selectElement.append(optionElement);
            }

        },
        error: function (xhr, status, error) {
            console.log("Errore durante il recupero degli eventi disponibili.");
        }
    });
}

getEventiDisponibili();
