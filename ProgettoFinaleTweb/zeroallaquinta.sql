-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Lug 08, 2023 alle 00:56
-- Versione del server: 10.4.28-MariaDB
-- Versione PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zeroallaquinta`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `eventi`
--

CREATE TABLE `eventi` (
  `NomeEvento` varchar(100) NOT NULL,
  `eventoID` int(10) NOT NULL,
  `pathLocandina` varchar(255) NOT NULL COMMENT 'Path dove trovare immagine locandina evento.\r\n',
  `DescrizioneEvento` varchar(1000) NOT NULL,
  `DataEvento` date NOT NULL,
  `PostiDisponibili` int(10) NOT NULL,
  `PostiTotali` int(10) NOT NULL,
  `LuogoEvento` varchar(100) NOT NULL,
  `MailResponsabile` varchar(62) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `personale`
--

CREATE TABLE `personale` (
  `Nome` varchar(50) NOT NULL,
  `Cognome` varchar(50) NOT NULL,
  `Mail` varchar(62) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Telefono` varchar(32) NOT NULL,
  `AnnoNascita` year(4) NOT NULL,
  `Ruolo` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `personale`
--

INSERT INTO `personale` (`Nome`, `Cognome`, `Mail`, `Password`, `Telefono`, `AnnoNascita`, `Ruolo`) VALUES
('b', 'b', 'b@allaquinta.it', '9a0b9d412a33ef7ab9dd11226d09fb26', '+393933226934', '1998', 'Admin');

-- --------------------------------------------------------

--
-- Struttura della tabella `spettatori`
--

CREATE TABLE `spettatori` (
  `IDprenotazione` int(100) NOT NULL,
  `Nome` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Cognome` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Mail` varchar(62) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Telefono` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `AnnoNascita` year(4) NOT NULL,
  `idPartecipazioneEvento` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Spettatori agli Eventi';

-- --------------------------------------------------------

--
-- Struttura della tabella `suggerimenti`
--

CREATE TABLE `suggerimenti` (
  `IdSuggerimento` int(10) NOT NULL,
  `MailSugg` varchar(62) NOT NULL COMMENT 'Chi ha mandato il suggerimento',
  `Testo` varchar(1500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `eventi`
--
ALTER TABLE `eventi`
  ADD PRIMARY KEY (`eventoID`),
  ADD KEY `MailResponsabile` (`MailResponsabile`);

--
-- Indici per le tabelle `personale`
--
ALTER TABLE `personale`
  ADD PRIMARY KEY (`Mail`);

--
-- Indici per le tabelle `spettatori`
--
ALTER TABLE `spettatori`
  ADD PRIMARY KEY (`IDprenotazione`),
  ADD KEY `idPartecipazioneEvento` (`idPartecipazioneEvento`);

--
-- Indici per le tabelle `suggerimenti`
--
ALTER TABLE `suggerimenti`
  ADD PRIMARY KEY (`IdSuggerimento`),
  ADD KEY `MailSugg` (`MailSugg`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `eventi`
--
ALTER TABLE `eventi`
  MODIFY `eventoID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT per la tabella `spettatori`
--
ALTER TABLE `spettatori`
  MODIFY `IDprenotazione` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT per la tabella `suggerimenti`
--
ALTER TABLE `suggerimenti`
  MODIFY `IdSuggerimento` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `eventi`
--
ALTER TABLE `eventi`
  ADD CONSTRAINT `eventi_ibfk_1` FOREIGN KEY (`MailResponsabile`) REFERENCES `personale` (`Mail`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `spettatori`
--
ALTER TABLE `spettatori`
  ADD CONSTRAINT `spettatori_ibfk_1` FOREIGN KEY (`idPartecipazioneEvento`) REFERENCES `eventi` (`eventoID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `suggerimenti`
--
ALTER TABLE `suggerimenti`
  ADD CONSTRAINT `suggerimenti_ibfk_1` FOREIGN KEY (`MailSugg`) REFERENCES `personale` (`Mail`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
