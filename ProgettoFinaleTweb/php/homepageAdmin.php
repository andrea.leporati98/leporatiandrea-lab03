<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Homepage Admin</title>
  <link rel="icon" href="../img/ZeroQuinta.png" type="image/png">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="../js/homepageAdmin.js" type="text/javascript"></script>
  <script src="../js/getEventi.js" type="text/javascript"></script>
  <link href="../css/homepageAdmin.css" type="text/css" rel="stylesheet">
</head>

<body>
  <?php
  if (!isset($_SESSION)) {
    session_start();
  }
  ?>
  <header>
    <?php if (isset($_SESSION["mail"])) { ?>
      <div id="user">
        <img src="../img/User.png" alt="Logo" width=40px height=40px>
        <p id="currentUser"><?= $_SESSION["mail"] ?></p>
      </div>
      <div id="popUpUser">
        <p id="logOut">LogOut</p>
      </div>
    <?php } ?>
    <h1>Area Amministratori</h1>
  </header>

  <div class="container">
    <div class="left-section">
      <!-- Sezione per la creazione di nuovi eventi -->
      <h2>Crea Nuovo Evento</h2>
      <form id="createEventForm" method="post">
        <!-- Campi del form per la creazione dell'evento -->
        <label for="eventName">Nome Evento:</label>
        <input type="text" id="eventName" name="eventName" required>

        <label for="eventPoster">Locandina Evento:</label>
        <input type="text" id="eventPoster" name="eventPoster" required>

        <label for="eventDescription">Descrizione:</label>
        <textarea id="eventDescription" name="eventDescription" required></textarea>

        <label for="eventDate">Data:</label>
        <input type="date" id="eventDate" name="eventDate" required>

        <label for="totalSeats">Posti Totali:</label>
        <input type="number" id="totalSeats" name="totalSeats" required>

        <label for="eventLocation">Luogo:</label>
        <input type="text" id="eventLocation" name="eventLocation" required>

        <label for="responsableEmail">Email Responsabile:</label>
        <select name="responsableEmail" id="responsableEmail"></select>

        <input type="submit" value="Crea Evento">
      </form>
    </div>

    <div class="third-section">
      <!-- Terza sezione a sinistra -->
      <h2>Gestisci Eventi</h2>
      <div id="eventListContainer"></div>
    </div>

    <div class="right-section">
      <!-- Sezione per la visualizzazione dei suggerimenti degli altri membri -->
      <h2>Suggerimenti degli Utenti</h2>
      <div id="suggerimentiContainer"></div>

    </div>
  </div>

  <footer>
    <p>&copy; 2023 Associazione Culturale</p>
  </footer>

</body>

</html>