<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SignUpZeroAllaQuinta</title>
    <link href="../css/signup.css" type="text/css" rel="stylesheet">
    <link rel="icon" href="../img/ZeroQuinta.png" type="image/png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../js/signupJson.js" type="text/javascript"></script>
</head>

<body>
    <?php
    if (!isset($_SESSION)) {
        session_start();
    }
    ?>
    <header>
        <h1>Zero alla quinta</h1>
    </header>
    <div id="divMessaggioRegistrazione" class="bacgroundPopUp">
        <div id="messaggioSignUp">
            <p id="testoMessaggioSignUp"></p>
            <button type="button" onclick="window.location.href = 'index.php';" id="redirectLogin" > Vai al Login </button>
        </div>
    </div>
    <!--<button type="button" id="bottoneReqPass">Mostra</button>-->
    <div id="container">
        <div id="areaSignup">
            <div id="SignupCol" class="bordered">
                <form id="registration" method="post">
                    <h3>Registrazione membri:</h3>
                    <div id="message"></div>
                    <label for="nome"><strong>Nome:</strong></label>
                    <input type="text" name="nome" size="16" id="nome" autofocus required><br><br>
                    <label for="cognome"><strong>Cognome:</strong></label>
                    <input type="text" name="cognome" size="16" id="cognome" autofocus required><br><br>
                    <label for="mail_prefix"><strong>Mail:</strong></label>
                    <input type="text" name="mail_prefix" size="16" id="mail_prefix" autofocus required>
                    <span >@allaquinta.it</span>
                    <?php if (isset($mail_error)) { ?>
                        <span style="color: red;"><?php echo $mail_error; ?></span>
                    <?php } ?>
                    <br><br>
                    <label for="pw"><strong>Password:</strong></label>
                    <div class="popup" id="requisitiPassword">
                        <input type="password" name="pw" id="pw" size="16" autofocus required>
                        <span class="popuptext" id="myPopup">La password deve contenere dagli 8 ai 16 caratteri, almeno un numero ed un carattere speciale</span>
                    </div><br><br>
                    <label for="pw2"><strong>Ripetere Password:</strong></label>
                    <input type="password" name="pw2" id="pw2" size="16" autofocus><br><br>
                    <label for="phone"><strong>Telefono:</strong></label>
                    <input type="tel" id="phone" name="phone" autofocus required> <br><br>
                    <label for="annoNascita"><strong>Anno di nascita:</strong></label>
                    <input type="number" name="annoNascita" id="annoNascita" size="4" autofocus min="1900" max="2099" required><br><br>
                    <label for="ruolo"><strong>Ruolo:</strong></label>
                    <select name="ruolo" id="ruolo">
                        <option value="Attore">Attore</option>
                        <option value="Musicista">Musicista</option>
                        <option value="Cantante">Cantante</option>
                        <option value="Tecnico ">Tecnico</option>
                        <option value="Manager Social">Manager Social</option>
                        <option value="Admin">Admin</option>
                    </select><br><br>
                    <input type="submit" value="Signup" id="signup">
                    <h4>Sei gia un membro?</h4>
                    <button type="button" onclick="window.location.href = 'index.php';"> Vai al Login </button>
                </form>

            </div>
        </div>
        <div id="LogoCol">
            <img src="../img/ZeroQuinta.png" alt="logo" class="centered" width="300px">
        </div>
    </div>
    <footer>
        <p>&copy; 2023 Associazione Culturale</p>
    </footer>
</body>

</html>