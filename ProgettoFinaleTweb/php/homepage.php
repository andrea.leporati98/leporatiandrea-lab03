<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>pagina personale</title>
    <link rel="icon" href="../img/ZeroQuinta.png" type="image/png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../js/homepage.js" type="text/javascript"></script>
    <script src="../js/getEventi.js" type="text/javascript"></script>
    <link href="../css/homepage.css" type="text/css" rel="stylesheet">
</head>

<body>
    <?php
    if (!isset($_SESSION)) {
        session_start();
    }
    ?>
    <header>
        <?php if (isset($_SESSION["mail"])) { ?>
            <div id="user">
                <img src="../img/User.png" alt="Logo" width=40px height=40px>
                <p id="currentUser"><?= $_SESSION["mail"] ?></p>
            </div>
            <div id="popUpUser">
                <p id="logOut">LogOut</p>
            </div>
        <?php } ?>
        <h1>Area Personale</h1>
    </header>
    <div class="container">
        <div class="left-section">
            <h2>Gestisci Eventi</h2>
            <div id="eventListContainer"></div>
        </div>

        <div class="right-section">
            <h2>Invia Suggerimento</h2>
            <form id="suggerimentoForm">
                <label for="suggestion">Suggerimento:</label>
                <textarea id="suggestion" placeholder="Inserisci il tuo suggerimento"></textarea>
                <input type="submit" value="InviaSuggerimento" id="InviaSuggerimento">
            </form>
        </div>
    </div>

    <footer>
        <p>&copy; 2023 Associazione Culturale</p>
    </footer>
</body>

</html>