<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Prenotazioni</title>
  <link href="../css/prenotazioni.css" type="text/css" rel="stylesheet">
  <link rel="icon" href="../img/ZeroQuinta.png" type="image/png">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="../js/carousel.js" type="text/javascript"></script>
  <script src="../js/prenotazioni.js" type="text/javascript"></script>
  <script src="../js/getEventiDisponibili.js" type="text/javascript"></script>

</head>

<body>
  <header>
    <h1>Prenotazioni</h1>
    <a href="index.php" class="homepage-button">Torna alla homepage</a>
  </header>
  <div class="slider-container">
    <div class="inner-slider">

    </div>
  </div>

  <div class="form-container">
  <form id="myForm">
    <label for="nome">Nome:</label>
    <input type="text" id="nome" name="nome" required>

    <label for="cognome">Cognome:</label>
    <input type="text" id="cognome" name="cognome" required>

    <label for="mail">Mail:</label>
    <input type="email" id="mail" name="mail" required>

    <label for="telefono">Telefono:</label>
    <input type="tel" id="telefono" name="telefono" required>

    <label for="annoNascita">Anno di Nascita:</label>
    <input type="number" id="annoNascita" name="annoNascita" required>

    <label for="posti">Numero di Posti:</label>
    <input type="number" id="posti" name="posti" required>

    <label for="evento">Evento:</label>
    <select id="evento" name="evento" required>
      <!-- Opzioni degli eventi disponibili -->
    </select>

    <input type="submit" value="Prenota">
  </form>
</div>


  <footer>
    <p>&copy; 2023 Associazione Culturale</p>
  </footer>
</body>

</html>