<?php
session_start(); // Avvia la sessione

// Effettua le operazioni di logout, ad esempio cancellando le variabili di sessione
$_SESSION = array(); // Cancella tutte le variabili di sessione
session_destroy(); // Termina la sessione

exit;
?>
