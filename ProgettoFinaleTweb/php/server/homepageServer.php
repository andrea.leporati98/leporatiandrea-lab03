<?php
include("logicaDatabase.php");

// Gestione richiesta di inserimento suggerimento
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    session_start();
    $mail = $_SESSION["mail"];
    $testo = $_POST["testo"];

    $query = "INSERT INTO suggerimenti (MailSugg, Testo) VALUES (?, ?)";

    $stmt = $db->prepare($query);
    $stmt->bindParam(1, $mail);
    $stmt->bindParam(2, $testo);

    if ($stmt->execute()) {
        $status = 1;
    } else {
        $status = 0;
    }

    echo json_encode(array("status" => $status));

} elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Gestione richiesta di recupero eventi
    $mailResponsabile = $_SESSION['mail']; // Otteniamo la mail dalla sessione

    $query = "SELECT * FROM eventi WHERE MailResponsabile = :mail";
    $stmt = $db->prepare($query);
    $stmt->bindParam(':mail', $mailResponsabile);
    $stmt->execute();

    $eventi = array();

    // Recupera i risultati della query
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $eventi[] = $row;
    }

    // Restituisci i dati degli eventi come JSON
    echo json_encode($eventi);
}
?>
