<?php
include("logicaDatabase.php");

// Esegui la query per ottenere i dati degli eventi disponibili
$query = "SELECT NomeEvento, eventoID, PostiDisponibili FROM eventi WHERE PostiDisponibili > 0";
$result = $db->query($query);

$eventi = array();

// Recupera i risultati della query
while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
  $nomeEvento = $row['NomeEvento'];
  $eventoID = $row['eventoID'];
  $postiDisponibili = $row['PostiDisponibili'];

  // Aggiungi il nome dell'evento, l'ID e i posti disponibili all'array
  $eventi[] = array(
    "NomeEvento" => $nomeEvento,
    "eventoID" => $eventoID,
    "PostiDisponibili" => $postiDisponibili
  );
}

// Restituisci i dati degli eventi disponibili come risposta JSON
echo json_encode($eventi);
?>
