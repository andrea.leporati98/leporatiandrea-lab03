<?php
  include("logicaDatabase.php");
  header("Content-type: application/json");
  if (isset($_POST["mail"]) && isset($_POST["pw"])) {
    $mailSessione = $_POST["mail"];
    $mail = $db->quote($_POST["mail"]);
    $password = $db->quote(md5($_POST["pw"]));
    if (!mailAlreadyInUse($mail)) {
      print "    {\"risposta\": \"utente non registrato\"}";
    }
    else {
    if (is_password_correct($mail, $password)) {
      if (isset($_SESSION)) {
        if (isset($_SESSION["currentPage"]))
          $currentPage = $_SESSION["currentPage"];
        else {
          $currentPage = NULL;
          unset($currentPage);
        }

        session_destroy();

      }

      $query = "SELECT Ruolo FROM personale WHERE Mail = $mail";
      $stmt = $db->prepare($query);
      $stmt->execute();

      if ($stmt->rowCount() > 0) {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $ruolo = $row['Ruolo'];
      }

      print "{\"risposta\": \"successo login\"";
      session_start();
      if (str_ends_with($mail, "allaquinta.it'")){
        if ($ruolo == "Admin") {
          $_SESSION["role"] = 'admin';
        print "    ,\"ruolo\": \"admin\"}";
        } else {
          $_SESSION["role"] = 'personale';    
        print "    ,\"ruolo\": \"personale\"}";
        }
        
      }
      $_SESSION["mail"] = $mailSessione;
    } 
    else
      print "    {\"risposta\": \"password errata\"}";
    }
  }
  ?>
