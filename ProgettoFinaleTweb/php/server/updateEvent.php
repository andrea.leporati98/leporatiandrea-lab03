<?php
include("logicaDatabase.php");

header("Content-type: application/json");

// Funzione per controllare i posti totali
function checkPostiTotali($postiDisponibili, $postiTotali) {
  if ($postiTotali >= $postiDisponibili) {
    return true; // I posti totali sono validi
  } else {
    return false; // I posti totali non sono validi
  }
}

// Recupera i dati inviati dal form
$nomeEvento = $_POST['nomeEvento'];
$eventoID = $_POST['eventoID'];
$pathLocandina = $_POST['pathLocandina'];
$descrizioneEvento = $_POST['descrizioneEvento'];
$dataEvento = $_POST['dataEvento'];
$postiDisponibili = $_POST['postiDisponibili'];
$postiTotali = $_POST['postiTotali'];
$luogoEvento = $_POST['luogoEvento'];

// Controlla i posti totali
if (!checkPostiTotali($postiDisponibili, $postiTotali)) {
  // I posti totali non sono validi, restituisci la risposta JSON di errore
  $response = array('status' => 'error', 'message' => 'I posti totali devono essere maggiori o uguali ai posti disponibili.');
  echo json_encode($response);
  exit; // Interrompi l'esecuzione dello script
}

// Esegui la query per aggiornare i dati dell'evento nel database
$query = "UPDATE eventi SET NomeEvento = ?, PathLocandina = ?, DescrizioneEvento = ?, DataEvento = ?, PostiDisponibili = ?, PostiTotali = ?, LuogoEvento = ? WHERE eventoID = ?";
$stmt = $db->prepare($query);
$stmt->bindParam(1, $nomeEvento);
$stmt->bindParam(2, $pathLocandina);
$stmt->bindParam(3, $descrizioneEvento);
$stmt->bindParam(4, $dataEvento);
$stmt->bindParam(5, $postiDisponibili);
$stmt->bindParam(6, $postiTotali);
$stmt->bindParam(7, $luogoEvento);
$stmt->bindParam(8, $eventoID);

if ($stmt->execute()) {
  // Operazione completata con successo
  $response = array('status' => 'success');
} else {
  // Errore durante l'aggiornamento dei dati dell'evento
  $response = array('status' => 'error');
}

echo json_encode($response);
?>
