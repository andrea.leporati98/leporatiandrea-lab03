<?php
include("logicaDatabase.php");

// Recupera i dati inviati dal form
$nome = $_POST['nome'];
$cognome = $_POST['cognome'];
$mail = $_POST['mail'];
$telefono = $_POST['telefono'];
$annoNascita = $_POST['annoNascita'];
$idPartecipazioneEvento = $_POST['idPartecipazioneEvento'];
$posti = $_POST['posti']; // Aggiunto il recupero dei posti riservati


// Esegui le operazioni necessarie con i dati della prenotazione

// Inserisci la prenotazione nel database
$query = "INSERT INTO spettatori (Nome, Cognome, Mail, Telefono, AnnoNascita, idPartecipazioneEvento)
          VALUES (?, ?, ?, ?, ?, ?)";
$stmt = $db->prepare($query);
$stmt->bindParam(1, $nome);
$stmt->bindParam(2, $cognome);
$stmt->bindParam(3, $mail);
$stmt->bindParam(4, $telefono);
$stmt->bindParam(5, $annoNascita);
$stmt->bindParam(6, $idPartecipazioneEvento);

if ($stmt->execute()) {
  // Verifica se il numero di posti riservati è maggiore dei posti disponibili
  $query = "SELECT PostiDisponibili FROM eventi WHERE eventoID = ?";
  $stmt = $db->prepare($query);
  $stmt->bindParam(1, $idPartecipazioneEvento);
  $stmt->execute();
  $postiDisponibili = $stmt->fetchColumn();

  if ($posti <= $postiDisponibili) {
    // Aggiorna il numero di posti disponibili dell'evento
    $query = "UPDATE eventi SET PostiDisponibili = PostiDisponibili - ? WHERE eventoID = ?";
    $stmt = $db->prepare($query);
    $stmt->bindParam(1, $posti); // Utilizza il numero di posti riservati
    $stmt->bindParam(2, $idPartecipazioneEvento);
    $stmt->execute();

    $response = array('status' => 'success');
  } else {
    $response = array('status' => 'error', 'message' => 'Il numero di posti selezionati supera i posti disponibili.');
  }
} else {
  $response = array('status' => 'error');
}

echo json_encode($response);