<?php
include("logicaDatabase.php");

header("Content-type: application/json");

$nome = $_POST["nome"];
$cognome = $_POST["cognome"];
$mail = $_POST["mail"];
$pw = md5($_POST["pw"]);
$telefono = $_POST["phone"];
$annoNascita = $_POST["annoNascita"];
$ruolo = $_POST["ruolo"];

$query = "INSERT INTO personale (Nome, Cognome, Mail, Password, Telefono, AnnoNascita, Ruolo) VALUES (?, ?, ?, ?, ?, ?, ?)";
$stmt = $db->prepare($query);
$stmt->bindParam(1, $nome);
$stmt->bindParam(2, $cognome);
$stmt->bindParam(3, $mail);
$stmt->bindParam(4, $pw);
$stmt->bindParam(5, $telefono);
$stmt->bindParam(6, $annoNascita);
$stmt->bindParam(7, $ruolo);

if ($stmt->execute()) {
    $status = 1;
} else {
    $status = 0;
}

echo json_encode(array("status" => $status));
?>
