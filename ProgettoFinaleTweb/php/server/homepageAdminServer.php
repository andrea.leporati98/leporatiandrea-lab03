<?php
include("logicaDatabase.php");
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Recupera i dati inviati dal form
    $eventName = $_POST['eventName'];
    $eventPoster = $_POST['eventPoster'];
    $eventDescription = $_POST['eventDescription'];
    $eventDate = $_POST['eventDate'];
    $availableSeats = $_POST['availableSeats'];
    $eventLocation = $_POST['eventLocation'];
    $responsableEmail = $_POST['responsableEmail'];
    $totalSeats = $_POST['totalSeats'];

    // Esegui la query per inserire l'evento nel database
    $query = "INSERT INTO eventi (NomeEvento, PathLocandina, DescrizioneEvento, DataEvento, PostiDisponibili, PostiTotali, LuogoEvento, MailResponsabile) 
              VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    $stmt = $db->prepare($query);
    $stmt->bindParam(1, $eventName);
    $stmt->bindParam(2, $eventPoster);
    $stmt->bindParam(3, $eventDescription);
    $stmt->bindParam(4, $eventDate);
    $stmt->bindParam(5, $availableSeats);
    $stmt->bindParam(6, $totalSeats);
    $stmt->bindParam(7, $eventLocation);
    $stmt->bindParam(8, $responsableEmail);

    if ($stmt->execute()) {
        // Operazione completata con successo
        $response = array('status' => 'success');
        echo json_encode($response);
    } else {
        // Errore durante l'inserimento dell'evento
        $response = array('status' => 'error');
        echo json_encode($response);
    }
    
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Gestione richiesta di recupero suggerimenti
    // Esegui la query per ottenere i suggerimenti dal database
    $query = "SELECT MailSugg, Testo FROM suggerimenti";
    $result = $db->query($query);

    $suggerimenti = array();

    // Recupera i risultati della query
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $mail = $row['MailSugg'];
        $testo = $row['Testo'];

        // Aggiungi i suggerimenti all'array
        $suggerimenti[] = array("mail" => $mail, "testo" => $testo);
    }

    // Restituisci i suggerimenti come risposta JSON
    echo json_encode($suggerimenti);
}
?>
