<?php
include("logicaDatabase.php");

// Esegui la query per ottenere i dati degli eventi
$query = "SELECT eventoID, NomeEvento, pathLocandina, DataEvento, PostiDisponibili, PostiTotali, DescrizioneEvento FROM eventi";
$result = $db->query($query);

$eventi = array();

// Recupera i risultati della query
while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
  $eventoID = $row['eventoID'];
  $nomeEvento = $row['NomeEvento'];
  $pathLocandina = $row['pathLocandina'];
  $dataEvento = $row['DataEvento'];
  $postiDisponibili = $row['PostiDisponibili'];
  $postiTotali = $row['PostiTotali'];
  $descrizioneEvento = $row['DescrizioneEvento']; // Aggiunta: Recupera la descrizione dell'evento

  // Aggiungi l'evento all'array
  $eventi[] = array(
    "eventoID" => $eventoID,
    "NomeEvento" => $nomeEvento,
    "pathLocandina" => $pathLocandina,
    "DataEvento" => $dataEvento,
    "PostiDisponibili" => $postiDisponibili,
    "PostiTotali" => $postiTotali,
    "DescrizioneEvento" => $descrizioneEvento // Aggiunta: Aggiungi la descrizione dell'evento
  );
}

// Restituisci i dati degli eventi come risposta JSON
echo json_encode($eventi);
?>
