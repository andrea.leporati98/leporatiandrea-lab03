<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ZeroAllaQuinta</title>
  <link rel="icon" href="../img/ZeroQuinta.png" type="image/png">
  <link href="../css/index.css" type="text/css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="../js/index.js" type="text/javascript"></script>
</head>

<body>
  <?php
  if (!isset($_SESSION)) {
    session_start();
  }
  ?>
  <header>
    <h1>Zero alla quinta</h1>
  </header>
  <div id="container">

    <div id="buttonContainer" class="bordered">
      <h3>Vuoi partecipare a un evento?</h3>
      <p>Clicca qua sotto per accedere alla pagina delle prenotazioni e dai un occhiata ai nostri eventi</p>
      <button class="button" onclick="window.location.href = 'prenotazioni.php';">Prenotazioni</button>
    </div>
    <div id="formContainer">

      <h3>Login per il personale</h3>
      <form id="formLogIn" method="post">
        <label for="mail"><strong>Mail:</strong></label>
        <input type="email" name="mail" id="mail" size="16" autofocus required><br><br>
        <label for="pw"><strong>Password:</strong></label>
        <input type="password" name="pw" id="pw" size="16" autofocus required><br>
        <p id="message" style="color:red; display:none"></p><br><br>
        <input type="submit" name="login" id="login"><br>
        <p>Vuoi fare parte dello staff? <a href="../php/signup.php">Registrati Ora!</a></p>
      </form>

    </div>
    <img src="../img/ZeroQuinta.png" alt="logo" class="centered" width="300px">
  </div>

  <footer>
    <p>&copy; 2023 Associazione Culturale</p>
  </footer>
</body>

</html>