STARRING:Ewan McGregor </br>Natalie Portman </br>Hayden Christensen </br>Ian McDiarmid </br>Samuel L. Jackson. </br>CHristopher Lee.
DIRECTOR:George Lucas
RATING:PG-13
THEATRICAL RELEASE:May 19, 2005
MOVIE SYNOPSIS:It has been three years since the Clone Wars began. Jedi Master Obi-Wan Kenobi (Ewan McGregor) and Jedi Knight Anakin Skywalker (Hayden Christensen) rescue Chancellor Palpatine (Ian McDiarmid) from General Grievous, the commander of the droid armies, but Grievous escapes. Suspicions are raised within the Jedi Council concerning Chancellor Palpatine, with whom Anakin has formed a bond. Asked to spy on the chancellor, and full of bitterness toward the Jedi Council, Anakin embraces the Dark Side.
DISTRIBUTOR:20th Century Fox
RUNTIME:2h 20m
GENRE:Sci-Fi, Adventure, Action, Fantasy
BOX OFFICE:$380.3M
