<!DOCTYPE html>

<html lang="en">

<head>
    <title>lab03</title>
    <link href="http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif" type="image/gif"
        rel="icon">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="movie.css" type="text/css" rel="stylesheet">
</head>

<body>
    <?php									
	$movie=$_GET["film"]; 
	$info = file($movie."/info.txt");
	$overview = file($movie."/overview.txt");
	$review = glob($movie."/review*.txt");
	$rvcounter = 1;
	$rvhalf = count($review)/2;
	$flag = true;
	?>

    <div id="banner">
        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png"
            alt="Rancid Tomatoes">
    </div>

    <?php
		$title = $info[0];
		$year = $info[1];
		?>
    <h1><?= $title?> ( <?= $year?>)</h1>

    <div id="main">
        
        <div id="right">
            <div>
                <?php						
					$overviewimage = $movie.'/overview.png';
				?>
                <img src=<?= $overviewimage?> alt="general overview">
            </div>

            <dl>
                <?php
				foreach($overview as $ovriga) {
					$ovarray = explode(':', $ovriga);
				?>
                <dt><?= $ovarray[0]?></dt>
                <dd><?= $ovarray[1]?></dd>
                <?php
				}
				?>
            </dl>

        </div> 
        <div id="left">
            <div id="left-top">
                <?php						
					$rating = (int) $info[2];
					if ($rating < 60) {
					?>

                <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rottenbig.png"
                    alt="Rotten">
                <span class="evaluation"><?= $rating?>% </span>

                <?php
					} else {
					?>

                <img src="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/freshbig.png" alt="Fresh">
                <span class="evaluation"><?= $rating?>% </span>

                <?php
					}
					?>
            </div>
            <div id="columns">
                <div id="leftcolumn">
                    <?php				
						foreach($review as $rvfile) {
							$rvlines = file($rvfile);
						?>
                    <p class="quotes">
                        <span>
                            <?php
								if (strpos($rvlines[1],"FRESH") !== false) {
								?>
                            <img src="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/fresh.gif"
                                alt="Fresh">
                            <?php
								} else {
								?>
                            <img src="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif"
                                alt="Rotten">
                            <?php
								}
								?>
                            <q> <?= $rvlines[0] ?> </q>
                        </span>
                    </p>
                    <p class="reviewers">
                        <img src="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/critic.gif"
                            alt="Critic">
                        <?= $rvlines[2] ?> <br>
                        <span class="publications"><?= $rvlines[3] ?></span>
                        <?php
						if ($rvcounter >= $rvhalf && $flag==true) {
						?>
                </div>
                <div id="rightcolumn">
				<?php $flag=false;
                	} else {
							$rvcounter = $rvcounter+1;
						}
					}
					?>
                </div>
            </div>
        </div>
        <p id="bottom">(1-<?=$rvcounter?>) of <?=$rvcounter?></p>

    </div>
    <div id="validators">
        <a href="http://validator.w3.org/check/referer">
            <img width="88" src="https://upload.wikimedia.org/wikipedia/commons/b/bb/W3C_HTML5_certified.png "
                alt="Valid HTML5!">
        </a>
        <br>
        <a href="http://jigsaw.w3.org/css-validator/check/referer">
            <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!">
        </a>
    </div>
</body>

</html>